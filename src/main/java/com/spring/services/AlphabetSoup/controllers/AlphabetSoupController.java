package com.spring.services.AlphabetSoup.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.spring.services.AlphabetSoup.services.AlphabetSoupService;

@RestController
public class AlphabetSoupController {
	
	@Autowired
	AlphabetSoupService alphabetSoupService;
	
	@PostMapping("/alphabet-soup")
	public List<String> processFileToGrid(@RequestParam(value = "file", required = true) MultipartFile file) throws Exception {
		return alphabetSoupService.processFileToGrid(file);
	}
}
