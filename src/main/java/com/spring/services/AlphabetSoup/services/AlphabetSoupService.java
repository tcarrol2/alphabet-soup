package com.spring.services.AlphabetSoup.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.spring.services.AlphabetSoup.domain.Grid;

@Service
public class AlphabetSoupService {

	// Get the file contents and process them into the Grid object
	public List<String> processFileToGrid(MultipartFile file) throws IOException {
		InputStream inputStream = file.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

		Grid grid = new Grid();
		String contentLine = reader.readLine();
		while (contentLine != null) {
			if (grid.getGridRows() == null) {
				String[] gridSize = contentLine.split("x");
				grid.setGridRows(Integer.parseInt(gridSize[0]));
				grid.setGridColumns(Integer.parseInt(gridSize[1]));
			} else if (contentLine.contains(" ")) {
				grid.getCharacterGrid().add(Arrays.asList(contentLine.split(" ")));
			} else {
				grid.getWordsToSearch().add(contentLine);
			}
			contentLine = reader.readLine();
		}
		searchGrid(grid);
		return grid.getWordsFound();
	}

	// Start searching for each word at all available characters in the grid.
	public void searchGrid(Grid grid) {
		IntStream.range(0, grid.getGridColumns())
				.forEach(column -> IntStream.range(0, grid.getGridRows()).forEach(row -> {
					grid.setWordFound(grid.getCharacterGrid().get(row).get(column));
					searchGridByColumnAndRow(grid, row, column);
				}));
	}

	// Knowing the location of the current character, walk the grid in all available directions.
	private void searchGridByColumnAndRow(Grid grid, int currentY, int currentX) {
		Multimap<Integer, Integer> validXYCoordinates = getValidXYCoordinates();
		validXYCoordinates.forEach((xCoordDirection, yCoordDirection) -> {
			// Start at once since we have the first character
			IntStream.range(1, Math.max(grid.getGridRows(), grid.getGridColumns())).forEach(walkGrid -> {
				int newX = (xCoordDirection * walkGrid) + currentX;
				int newY = (yCoordDirection * walkGrid) + currentY;
				// Always start with the first letter found in the method searchGrid
				if (newX >= 0 && newY >= 0 && newX < grid.getGridColumns() && newY < grid.getGridRows()) {
					grid.setWordFound(grid.getWordFound() + grid.getCharacterGrid().get(newY).get(newX));
					if (grid.getWordsToSearch().contains(grid.getWordFound())) {
						grid.getWordsFound().add(grid.getWordFound() + " " + currentY + ":" + currentX + " " + newY + ":" + newX);
					}
					// Reset the word found to first letter if word length is larger than possible from the grid
					if (grid.getWordFound().length() > grid.getGridColumns() && grid.getWordFound().length() > grid.getGridRows()) {
						grid.setWordFound(grid.getWordFound().substring(0, 1));
					}
				} else {
					return;
				}
			});
			grid.setWordFound(grid.getWordFound().substring(0, 1));
		});
		grid.setWordFound(null);
	}

	private Multimap<Integer, Integer> getValidXYCoordinates() {
		Multimap<Integer, Integer> validXYCoordinates = ArrayListMultimap.create();
		validXYCoordinates.put(-1, -1);
		validXYCoordinates.put(-1, 0);
		validXYCoordinates.put(-1, 1);
		validXYCoordinates.put(0, -1);
		validXYCoordinates.put(0, 1);
		validXYCoordinates.put(1, -1);
		validXYCoordinates.put(1, 0);
		validXYCoordinates.put(1, 1);
		return validXYCoordinates;
	}
}