package com.spring.services.AlphabetSoup.domain;

import java.util.ArrayList;
import java.util.List;

public class Grid {

	private String wordFound;
	private Integer gridRows;
	private Integer gridColumns;
	private List<List<String>> characterGrid = new ArrayList<List<String>>();
	private List<String> wordsToSearch = new ArrayList<String>();
	private List<String> wordsFound = new ArrayList<String>();

	public String getWordFound() {
		return wordFound;
	}

	public void setWordFound(String wordFound) {
		this.wordFound = wordFound;
	}

	public Integer getGridRows() {
		return gridRows;
	}

	public void setGridRows(Integer gridRows) {
		this.gridRows = gridRows;
	}

	public Integer getGridColumns() {
		return gridColumns;
	}

	public void setGridColumns(Integer gridColumns) {
		this.gridColumns = gridColumns;
	}

	public List<List<String>> getCharacterGrid() {
		return characterGrid;
	}

	public void setCharacterGrid(List<List<String>> characterGrid) {
		this.characterGrid = characterGrid;
	}

	public List<String> getWordsToSearch() {
		return wordsToSearch;
	}

	public void setWordsToSearch(List<String> wordsToSearch) {
		this.wordsToSearch = wordsToSearch;
	}

	public List<String> getWordsFound() {
		return wordsFound;
	}

	public void setWordsFound(List<String> wordsFound) {
		this.wordsFound = wordsFound;
	}
}
