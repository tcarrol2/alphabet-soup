package test.com.spring.services.AlphabetSoup.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.spring.services.AlphabetSoup.AlphabetSoupApplication;
import com.spring.services.AlphabetSoup.domain.Grid;
import com.spring.services.AlphabetSoup.services.AlphabetSoupService;

@SpringBootTest(classes = AlphabetSoupApplication.class)
public class TestAlphabetSoupService {
	
	@Autowired
	AlphabetSoupService alphabetSoupService;
	
	@Test
	public void TestProcessFileToGrid() throws IOException {
		Path path = Paths.get("src/test/resources/test.txt");
		String name = "test.txt";
		String originalFileName = "test.txt";
		String contentType = "text/plain";
		byte[] content = Files.readAllBytes(path);
		MultipartFile result = new MockMultipartFile(name, originalFileName, contentType, content);
		
		List<String> foundWords = alphabetSoupService.processFileToGrid(result);
		assertEquals("[GOOD 4:0 4:3, TOUCH 4:4 0:0]", foundWords.toString());
	}
	
	@Test
	public void TestSearchGridOne() {
		Grid grid = new Grid();
		grid.setGridColumns(5);
		grid.setGridRows(5);
		grid.setWordsToSearch(Arrays.asList("HELLO", "GOOD", "BYE"));
		List<String> firstCharacters = Arrays.asList("H", "A", "S", "D", "F");
		grid.getCharacterGrid().add(firstCharacters);
		List<String> secondCharacters = Arrays.asList("G", "E", "Y", "B", "H");
		grid.getCharacterGrid().add(secondCharacters);
		List<String> thirdCharacters = Arrays.asList("J", "K", "L", "Z", "X");
		grid.getCharacterGrid().add(thirdCharacters);
		List<String> fourthCharacters = Arrays.asList("C", "V", "B", "L", "N");
		grid.getCharacterGrid().add(fourthCharacters);
		List<String> fifthCharacters = Arrays.asList("G", "O", "O", "D", "O");
		grid.getCharacterGrid().add(fifthCharacters);
		
		alphabetSoupService.searchGrid(grid);
		
		assertEquals("[HELLO 0:0 4:4, GOOD 4:0 4:3, BYE 1:3 1:1]", grid.getWordsFound().toString());
	}

	@Test
	public void TestSearchGridTwo() {
		Grid grid = new Grid();
		grid.setGridColumns(3);
		grid.setGridRows(3);
		grid.setWordsToSearch(Arrays.asList("ABC", "AEI"));
		List<String> firstCharacters = Arrays.asList("A", "B", "C");
		grid.getCharacterGrid().add(firstCharacters);
		List<String> secondCharacters = Arrays.asList("D", "E", "F");
		grid.getCharacterGrid().add(secondCharacters);
		List<String> thirdCharacters = Arrays.asList("G", "H", "I");
		grid.getCharacterGrid().add(thirdCharacters);
		
		alphabetSoupService.searchGrid(grid);

		assertEquals("[ABC 0:0 0:2, AEI 0:0 2:2]", grid.getWordsFound().toString());
	}

}
